<?php

/**
 * 361GRAD Element Googlemapteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = "DSE-Elements";
$GLOBALS['TL_LANG']['CTE']['dse_googlemapteaser'] = ["Google Map", "Google Map"];

$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_latitude'] =
    ['Latitude', 'Please enter the latitude value.'];
$GLOBALS['TL_LANG']['tl_content']['dse_longitude'] =
    ['Longitude', 'Please enter the Longitude value.'];
$GLOBALS['TL_LANG']['tl_content']['dse_maplink'] =
    ['Roadmap link', 'Here you can add a roadmap link.'];
$GLOBALS['TL_LANG']['tl_content']['dse_mapmarker'] =
    ['Map marker image', 'Here you can add a map marker.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];