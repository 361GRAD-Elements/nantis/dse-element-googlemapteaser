<?php

/**
 * 361GRAD Element Googlemapteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_googlemapteaser'] =
    '{type_legend},type,headline,dse_latitude,dse_longitude,dse_maplink,dse_mapmarker;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop,dse_data';

// Element fields
    '{type_legend},type,headline,dse_maplink;' .
    '{invisible_legend:hide},invisible,start,stop,dse_data';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_latitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_latitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_longitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_longitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_maplink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_maplink'],
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => [
        'allowHtml'=>true,
        'rte'=>'ace|html',
        'class'=>'monospace',
        'tl_class'  => 'clr w100'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_mapmarker'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_mapmarker'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'w50 clr',
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];